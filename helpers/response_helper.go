package helpers

import (
	"calculator_server/models"
	"encoding/json"
	"fmt"
	"github.com/labstack/echo/v4"
	"io/ioutil"
)

// Response function response back to caller
func Response(ctx echo.Context,statusCode int, data interface{}) error {
	ctx.Response().Header().Set(echo.HeaderContentType, echo.MIMEApplicationJSONCharsetUTF8)
	ctx.Response().WriteHeader(statusCode)
	return json.NewEncoder(ctx.Response()).Encode(data)
}

// ReadBody read request body marshal to struct
func ReadBody(ctx echo.Context) (*models.CalculatorRequestModel, error) {
	body, err := ioutil.ReadAll(ctx.Request().Body)
	if err != nil {
		ctx.Echo().Logger.Error("fail to read request body")
	}

	objBody := new(models.CalculatorRequestModel)
	err = json.Unmarshal(body, objBody)
	if err != nil {
		return nil, fmt.Errorf("error while unmarshal data: %v", err)
	}

	return objBody, nil
}
