package applications

import (
	"calculator_server/controllers"
	"os"

	"github.com/joho/godotenv"

	"github.com/labstack/echo/v4"
)

type AppInit struct {
	echo             *echo.Echo
	searchController controllers.ICalculator
}

func (a *AppInit) Start() {
	err := godotenv.Load()
	if err != nil {
		a.echo.Logger.Fatal("Error loading .env file")
	}

	a.searchController.Handle()
	a.echo.Logger.Fatal(a.echo.Start(os.Getenv("SERVICE_PORT")))
}

func NewApp(e *echo.Echo, con controllers.ICalculator) AppInit {
	return AppInit{
		echo:             e,
		searchController: con,
	}
}
