// +build wireinject

package main

import (
	"calculator_server/applications"
	"calculator_server/controllers"
	"calculator_server/services"

	"github.com/labstack/echo/v4"

	"github.com/google/wire"
)

var superSet = wire.NewSet(
	controllers.NewCalcClient,
	services.NewService,
	applications.NewApp,
)

// Initialize way 1
func Initialize(e *echo.Echo) applications.AppInit {
	wire.Build(superSet)
	return applications.AppInit{}
}
