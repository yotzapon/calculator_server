package controllers

import (
	"calculator_server/helpers"
	"calculator_server/models"
	"calculator_server/services"
	"fmt"
	"math"
	"net/http"

	"github.com/labstack/echo/v4"
)

const (
	SUM_OPERATION = "Summation [+]"
	SUB_OPERATION = "Subtraction [-]"
	MUL_OPERATION = "Multiple [*]"
	DIV_OPERATION = "Division [/]"
)

// ICalculator interface for calculate functional
type ICalculator interface {
	Sum(ctx echo.Context) error
	Mul(ctx echo.Context) error
	Sub(ctx echo.Context) error
	Div(ctx echo.Context) error
	Handle()
}

// CalcClient receiver for calculate
type CalcClient struct {
	echo    *echo.Echo
	model   *models.CalculatorRequestModel
	service services.ICalculateService
}

// NewCalcClient for create CalcClient
func NewCalcClient(e *echo.Echo, service services.ICalculateService) ICalculator {
	return &CalcClient{model: new(models.CalculatorRequestModel), echo: e, service: service}
}

// Handle handle function according the route called by user
func (cal *CalcClient) Handle() {
	cal.echo.GET("/", func(context echo.Context) error {
		return context.String(http.StatusOK, "service's healthy!")
	})

	cal.echo.POST("/calculator.sum", cal.Sum).Name = "sum"
	cal.echo.POST("/calculator.mul", cal.Mul).Name = "mul"
	cal.echo.POST("/calculator.sub", cal.Sub).Name = "sub"
	cal.echo.POST("/calculator.div", cal.Div).Name = "div"

}

// Sum summation passed request
func (cal *CalcClient) Sum(ctx echo.Context) error {
	ctx.Logger().Info("calculate summation begin")

	var err error
	cal.model, err = helpers.ReadBody(ctx)
	if err != nil {
		ctx.Logger().Errorf("something went wrong while reading request body: %v", err)
		return helpers.Response(ctx, http.StatusBadRequest, err)
	}

	result := cal.service.Sum(cal.model)
	body := &models.CalculatorResponseModel{
		Operand:  fmt.Sprintf("A: %v, B: %v", cal.model.A, cal.model.B),
		Operator: SUM_OPERATION,
		Result:   result,
	}
	ctx.Logger().Info("calculate summation done")
	return helpers.Response(ctx, http.StatusOK, body)
}

// Mul multiple passed request
func (cal *CalcClient) Mul(ctx echo.Context) error {
	ctx.Logger().Info("calculate multiple begin")
	var err error
	cal.model, err = helpers.ReadBody(ctx)
	if err != nil {
		ctx.Logger().Errorf("something went wrong while reading request body: %v", err)
		return helpers.Response(ctx, http.StatusBadRequest, err)
	}

	result := cal.service.Mul(cal.model)
	body := &models.CalculatorResponseModel{
		Operand:  fmt.Sprintf("A: %v, B: %v", cal.model.A, cal.model.B),
		Operator: MUL_OPERATION,
		Result:   result,
	}
	ctx.Logger().Info("calculate multiple done")
	return helpers.Response(ctx, http.StatusOK, body)
}

// Sub subtraction passed request
func (cal *CalcClient) Sub(ctx echo.Context) error {
	ctx.Logger().Info("calculate subtraction begin")
	var err error
	cal.model, err = helpers.ReadBody(ctx)
	if err != nil {
		ctx.Logger().Errorf("something went wrong while reading request body: %v", err)
		return helpers.Response(ctx, http.StatusBadRequest, err)
	}

	result := cal.service.Sub(cal.model)
	body := &models.CalculatorResponseModel{
		Operand:  fmt.Sprintf("A: %v, B: %v", cal.model.A, cal.model.B),
		Operator: SUB_OPERATION,
		Result:   result,
	}
	ctx.Logger().Info("calculate subtraction done")
	return helpers.Response(ctx, http.StatusOK, body)
}

// Div division passed request
func (cal *CalcClient) Div(ctx echo.Context) error {
	ctx.Logger().Info("calculate division begin")
	var err error
	cal.model, err = helpers.ReadBody(ctx)
	if err != nil {
		ctx.Logger().Errorf("something went wrong while reading request body: %v", err)
		return helpers.Response(ctx, http.StatusBadRequest, err)
	}

	if cal.model.B == 0 {
		return helpers.Response(ctx, http.StatusBadRequest, "divisor(B) should not be zero")
	}

	var result = cal.service.Div(cal.model)
	body := &models.CalculatorResponseModel{
		Operand:  fmt.Sprintf("A: %v, B: %v", cal.model.A, cal.model.B),
		Operator: DIV_OPERATION,
		Result:   math.Round(result * 100 / 100),
	}
	ctx.Logger().Info("calculate division done")
	return helpers.Response(ctx, http.StatusOK, body)
}
