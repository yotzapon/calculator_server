package controllers

import (
	mock_services "calculator_server/services/mocks"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/labstack/echo/v4"

	"github.com/golang/mock/gomock"
)

func TestHandleSum_Success(t *testing.T) {
	assert := assert.New(t)
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockServe := mock_services.NewMockICalculateService(mockCtrl)
	mockServe.EXPECT().Sum(gomock.Any()).Return(float64(10)).Times(1)
	e := echo.New()
	cont := NewCalcClient(e, mockServe)
	e.POST("/calculator.sum", cont.Sum).Name = "sum"

	c, b := request(http.MethodPost, "/calculator.sum", e, mockRequestSuccess())
	assert.Equal(http.StatusOK, c, "they should be equal")
	assert.NotEmpty(b)
}

func TestHandleSum_Fail(t *testing.T) {
	assert := assert.New(t)
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockServe := mock_services.NewMockICalculateService(mockCtrl)
	e := echo.New()
	cont := NewCalcClient(e, mockServe)
	e.POST("/calculator.sum", cont.Sum).Name = "sum"

	c, b := request(http.MethodPost, "/calculator.sum", e, mockRequestFailure())
	assert.Equal(http.StatusBadRequest, c, "they should be equal")
	assert.NotEmpty(b)
}

func TestHandleMul_Success(t *testing.T) {
	assert := assert.New(t)
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockServe := mock_services.NewMockICalculateService(mockCtrl)
	mockServe.EXPECT().Mul(gomock.Any()).Return(float64(10)).Times(1)
	e := echo.New()
	cont := NewCalcClient(e, mockServe)
	e.POST("/calculator.mul", cont.Mul).Name = "mul"

	c, b := request(http.MethodPost, "/calculator.mul", e, mockRequestSuccess())
	assert.Equal(http.StatusOK, c, "they should be equal")
	assert.NotEmpty(b)
}

func TestHandleSub_Success(t *testing.T) {
	assert := assert.New(t)
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockServe := mock_services.NewMockICalculateService(mockCtrl)
	mockServe.EXPECT().Sub(gomock.Any()).Return(float64(10)).Times(1)
	e := echo.New()
	cont := NewCalcClient(e, mockServe)
	e.POST("/calculator.sub", cont.Sub).Name = "sub"

	c, b := request(http.MethodPost, "/calculator.sub", e, mockRequestSuccess())
	assert.Equal(http.StatusOK, c, "they should be equal")
	assert.NotEmpty(b)
}

func TestHandleDiv_Success(t *testing.T) {
	assert := assert.New(t)
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockServe := mock_services.NewMockICalculateService(mockCtrl)
	mockServe.EXPECT().Div(gomock.Any()).Return(float64(10)).Times(1)
	e := echo.New()
	cont := NewCalcClient(e, mockServe)
	e.POST("/calculator.div", cont.Div).Name = "div"

	c, b := request(http.MethodPost, "/calculator.div", e, mockRequestSuccess())
	assert.Equal(http.StatusOK, c, "they should be equal")
	assert.NotEmpty(b)
}

func TestHandleDiv_Failure(t *testing.T) {
	assert := assert.New(t)
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockServe := mock_services.NewMockICalculateService(mockCtrl)
	e := echo.New()
	cont := NewCalcClient(e, mockServe)
	e.POST("/calculator.div", cont.Div).Name = "div"

	c, b := request(http.MethodPost, "/calculator.div", e, mockRequestDivZero())
	assert.Equal(http.StatusBadRequest, c, "they should be equal")
	assert.NotEmpty(b)
}
func TestHandleFunc(t *testing.T) {
	// mockServe := mock_services.NewMockICalculateService(mockCtrl)
	e := echo.New()
	cont := NewCalcClient(e, nil)
	cont.Handle()

	type route struct {
		Method string
		Path   string
		Name   string
	}
	var r []route
	listFuncName := "mul_sum_sub_div"
	data, _ := json.MarshalIndent(e.Routes(), "", "  ")
	_ = json.Unmarshal(data, &r)

	for _, v := range r {
		if v.Method != http.MethodGet {
			if !strings.Contains(listFuncName, v.Name) {
				t.Fatalf("actual [%v] doesnt meet expected", v.Name)
			}
		}
	}
}

func request(method, path string, e *echo.Echo, body io.Reader) (int, string) {
	req := httptest.NewRequest(method, path, body)
	rec := httptest.NewRecorder()
	e.ServeHTTP(rec, req)
	return rec.Code, rec.Body.String()
}

func mockRequestSuccess() io.Reader {
	b := `{
	    "a": 100,
	    "b": 200.2
	}`
	return strings.NewReader(b)
}

func mockRequestDivZero() io.Reader {
	b := `{
	    "a": 100,
	    "b": 0
	}`
	return strings.NewReader(b)
}

func mockRequestFailure() io.Reader {
	b := `{
	    "a: 100,
	    "b": 200.2
	}`
	return strings.NewReader(b)
}
