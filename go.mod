module calculator_server

go 1.14

require (
	github.com/golang/mock v1.4.3
	github.com/google/wire v0.4.0
	github.com/joho/godotenv v1.3.0
	github.com/labstack/echo/v4 v4.1.16
	github.com/stretchr/testify v1.6.1
)
