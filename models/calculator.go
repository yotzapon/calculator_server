package models

type CalculatorRequestModel struct {
	A float32 `json:"a"`
	B float32 `json:"b"`
}

type CalculatorResponseModel struct {
	Operand  string  `json:"operand"`
	Operator string  `json:"operator"`
	Result   float64 `json:"result"`
}