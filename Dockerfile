FROM golang:latest
LABEL maintainer="Yotsapon L."

ENV SERVICE_PORT 8081
ENV WRKDIR /app/calculator
ENV APPNAME calculator_server
WORKDIR $WRKDIR
COPY . $WRKDIR

# Build the Go app
RUN go get ./...
RUN go test ./... -cover
RUN go build -o $APPNAME .

# Expose port 8080 to the outside world
EXPOSE $SERVICE_PORT

# Command to run the executable
CMD ["./calculator_server"]