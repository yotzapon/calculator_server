package main

import "github.com/labstack/echo/v4"

func main() {
	app := Initialize(echo.New())
	app.Start()
}
