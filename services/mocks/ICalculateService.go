// Code generated by MockGen. DO NOT EDIT.
// Source: ./calculator.go

// Package mock_services is a generated GoMock package.
package mock_services

import (
	models "calculator_server/models"
	gomock "github.com/golang/mock/gomock"
	reflect "reflect"
)

// MockICalculateService is a mock of ICalculateService interface
type MockICalculateService struct {
	ctrl     *gomock.Controller
	recorder *MockICalculateServiceMockRecorder
}

// MockICalculateServiceMockRecorder is the mock recorder for MockICalculateService
type MockICalculateServiceMockRecorder struct {
	mock *MockICalculateService
}

// NewMockICalculateService creates a new mock instance
func NewMockICalculateService(ctrl *gomock.Controller) *MockICalculateService {
	mock := &MockICalculateService{ctrl: ctrl}
	mock.recorder = &MockICalculateServiceMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockICalculateService) EXPECT() *MockICalculateServiceMockRecorder {
	return m.recorder
}

// Sum mocks base method
func (m *MockICalculateService) Sum(input *models.CalculatorRequestModel) float64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Sum", input)
	ret0, _ := ret[0].(float64)
	return ret0
}

// Sum indicates an expected call of Sum
func (mr *MockICalculateServiceMockRecorder) Sum(input interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Sum", reflect.TypeOf((*MockICalculateService)(nil).Sum), input)
}

// Mul mocks base method
func (m *MockICalculateService) Mul(input *models.CalculatorRequestModel) float64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Mul", input)
	ret0, _ := ret[0].(float64)
	return ret0
}

// Mul indicates an expected call of Mul
func (mr *MockICalculateServiceMockRecorder) Mul(input interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Mul", reflect.TypeOf((*MockICalculateService)(nil).Mul), input)
}

// Sub mocks base method
func (m *MockICalculateService) Sub(input *models.CalculatorRequestModel) float64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Sub", input)
	ret0, _ := ret[0].(float64)
	return ret0
}

// Sub indicates an expected call of Sub
func (mr *MockICalculateServiceMockRecorder) Sub(input interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Sub", reflect.TypeOf((*MockICalculateService)(nil).Sub), input)
}

// Div mocks base method
func (m *MockICalculateService) Div(input *models.CalculatorRequestModel) float64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Div", input)
	ret0, _ := ret[0].(float64)
	return ret0
}

// Div indicates an expected call of Div
func (mr *MockICalculateServiceMockRecorder) Div(input interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Div", reflect.TypeOf((*MockICalculateService)(nil).Div), input)
}
