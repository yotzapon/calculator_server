package services

import (
	"calculator_server/models"
	"testing"
)

func Test_Sum(t *testing.T) {
	clientServ := &CalcService{}
	rs := clientServ.Sum(&models.CalculatorRequestModel{A: 5, B: 5})
	if rs != 10 {
		t.Fatalf("expectd result: 10, actual: %v", rs)
	}
}

func Test_Mul(t *testing.T) {
	clientServ := &CalcService{}
	rs := clientServ.Mul(&models.CalculatorRequestModel{A: 5, B: 5})
	if rs != 25 {
		t.Fatalf("expectd result: 10, actual: %v", rs)
	}
}

func Test_Sub(t *testing.T) {
	clientServ := &CalcService{}
	rs := clientServ.Sub(&models.CalculatorRequestModel{A: 5, B: 5})
	if rs != 0 {
		t.Fatalf("expectd result: 10, actual: %v", rs)
	}
}

func Test_Div(t *testing.T) {
	clientServ := &CalcService{}
	rs := clientServ.Div(&models.CalculatorRequestModel{A: 10, B: 5})
	if rs != 2 {
		t.Fatalf("expectd result: 10, actual: %v", rs)
	}
}
