//go:generate mockgen -source=./calculator.go -destination=./mocks/ICalculateService.go

package services

import (
	"calculator_server/models"
)

// ICalculateService calculator service interface
type ICalculateService interface {
	Sum(input *models.CalculatorRequestModel) float64
	Mul(input *models.CalculatorRequestModel) float64
	Sub(input *models.CalculatorRequestModel) float64
	Div(input *models.CalculatorRequestModel) float64
}

// CalcService struct receiver to call function
type CalcService struct{}

// NewService
func NewService() ICalculateService {
	return &CalcService{}
}

// Sum summation function
func (c *CalcService) Sum(input *models.CalculatorRequestModel) float64 {
	return float64(input.A + input.B)
}

// Mul multiple function
func (c *CalcService) Mul(input *models.CalculatorRequestModel) float64 {
	return float64(input.A * input.B)
}

// Sub subtraction function
func (c *CalcService) Sub(input *models.CalculatorRequestModel) float64 {
	return float64(input.A - input.B)
}

// Div division function
func (c *CalcService) Div(input *models.CalculatorRequestModel) float64 {
	return float64(input.A / input.B)
}
